package nl.itris.viewpoint.document.client.rest;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Response;

import nl.itris.viewpoint.document.client.entities.EditableDocument;

public class RESTReceiveEditableDocument {
	
    public static EditableDocument consumeWithJsonb(String targetUrl) {
    	
        Client client = ClientBuilder.newClient();
        Response response = client.target(targetUrl).request().get();
        
        EditableDocument editableDocument = response.readEntity(EditableDocument.class);

        response.close();
        client.close();

        return editableDocument;
        
      }
	
}
