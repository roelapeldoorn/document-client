package nl.itris.viewpoint.document.client.programme;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SpringLayout;
import javax.swing.SwingUtilities;

import org.apache.commons.configuration.Configuration;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import nl.itris.viewpoint.document.client.config.ClientProperties;
import nl.itris.viewpoint.document.client.entities.EditableDocument;
import nl.itris.viewpoint.document.client.rest.RESTReceiveEditableDocument;
import nl.itris.viewpoint.document.client.rest.RESTSendDocument;
import nl.itris.viewpoint.document.client.springutilities.SpringUtilities;

public class DocumentClient extends JPanel implements ActionListener, FocusListener {

	JTextField jtSeperator;
	JTextField jtLogging;

	JLabel jlStatus;
	JLabel jlInfo;

	JTextField jtDocumentname;
	JTextField jtDocumentSearchFor;
	JTextField jtURL;
	JTextField jtDownloadsFolder;
	JTextField jtWorkFolder;

	JFileChooser jfDownloadsFolder;
	String sDownloadsFolder;

	JFileChooser jfWorkFolder;
	String sWorkFolder;

	WatchService downloadWatcher;
	WatchService workWatcher;

	private List<EditableDocument> listOfEditableDocuments = new ArrayList<EditableDocument>();
	private EditableDocument currentEditableDocument;

	final static int GAP = 10;
	private static final long serialVersionUID = 1L;

	private volatile Thread downloadWatcherThread;
	private volatile Thread workWatcherThread;

	public DocumentClient() {

		jtSeperator = new JTextField();
		jtSeperator.setText(FileSystems.getDefault().getSeparator());

		jtLogging = new JTextField();
		jtLogging.setText("true");

		jlInfo = new JLabel();
		jlInfo.setText("");
		
		writeLogging("Separator", jtSeperator.getText());

		setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));

		JPanel jpInstellingen = new JPanel() {

			private static final long serialVersionUID = 6450134821346379991L;

			public Dimension getMaximumSize() {
				Dimension pref = getPreferredSize();
				return new Dimension(Integer.MAX_VALUE, pref.height);
			}
		};

		jpInstellingen.setLayout(new BoxLayout(jpInstellingen, BoxLayout.PAGE_AXIS));
		jpInstellingen.add(createEntryFields());
		jpInstellingen.add(createButtons());
		jpInstellingen.add(createStatusBar());

		add(jpInstellingen);

	}

	public void startDownloadWatcherThread() {

		downloadWatcherThread = new Thread() {
			public void run() {
				try {
					processDownloadNotifications();
				} catch (Exception ie) {
					ie.printStackTrace();
				}
				downloadWatcherThread = null;
			}
		};
		downloadWatcherThread.start();
		
	}

	public void processDownloadNotifications() {

		try {

			Path downloadDirectory = Paths.get(jtDownloadsFolder.getText());
			downloadWatcher = FileSystems.getDefault().newWatchService();
			WatchKey downloadKey = downloadDirectory.register(downloadWatcher, StandardWatchEventKinds.ENTRY_CREATE);

			while ((downloadKey = downloadWatcher.take()) != null) {
				for (WatchEvent<?> event : downloadKey.pollEvents()) {
					writeLogging("Download event kind", event.kind() + ". File affected: " + event.context());
					processDownloadEvent(event.context().toString());

				}
				downloadKey.reset();
			}

		} catch (IOException ioe) {
			ioe.printStackTrace();
		} catch (InterruptedException ie) {
			ie.printStackTrace();
		}

	}

	public void processDownloadEvent(String documentnameDownload) {

		if (!documentnameDownload.endsWith("xxxxx.workfile")) {
			return;
		}

		jlStatus.setText("Document verwerken ...");

		jtDocumentSearchFor.setText(documentnameDownload);

		documentnameDownload = documentnameDownload.replaceAll("_dubbelepunt_", ":").replaceAll("_slash_", "/")
				.replaceAll("_space_", " ");

		String[] documentNameSplitted = new String[3];

		try {
			documentNameSplitted = documentnameDownload.split("xxxxx");
		} catch (Exception e) {
		}

		if (!".workfile".equals(documentNameSplitted[2])) {
			return;
		}

		writeLogging("Aantal delen", documentNameSplitted.length);
		writeLogging("Deel 1", documentNameSplitted[0]);
		writeLogging("Deel 2", documentNameSplitted[1]);
		writeLogging("Deel 3", documentNameSplitted[2]);

		jtURL.setText(documentNameSplitted[0]);

		EditableDocument downloadedEditableDocument = new EditableDocument();
		try {
			
			writeLogging("URL check", jtURL.getText() + "REST/documents/check/" + jtDocumentSearchFor.getText());
			
			downloadedEditableDocument = RESTReceiveEditableDocument.consumeWithJsonb(
					jtURL.getText() + "REST/documents/check/" + jtDocumentSearchFor.getText());
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (null != downloadedEditableDocument.getDocumentname()) {

			boolean newDocument = true;

			if (this.listOfEditableDocuments != null) {
				for (int i = 0; i < this.listOfEditableDocuments.size(); i++) {
					EditableDocument editableDocumentInList = listOfEditableDocuments.get(i);
					if (downloadedEditableDocument.getId() == editableDocumentInList.getId()) {
						newDocument = false;
					}
				}
			}

			this.currentEditableDocument = downloadedEditableDocument;

			writeLogging("Documenten in lijst", this.listOfEditableDocuments.size());
			writeLogging("Document komt uit ViewPoint", this.currentEditableDocument.getDocumentname());
			writeLogging("Document komt uit tabel", this.currentEditableDocument.getDocumentOriginalTable());
			writeLogging("Document key", this.currentEditableDocument.getDocumentOriginalID());

			jtDocumentname.setText(this.currentEditableDocument.getDocumentname());

			try {
				File fileTo = new File(jtWorkFolder.getText() + jtSeperator.getText()
						+ this.currentEditableDocument.getDocumentname());
				fileTo.delete();
			} catch (Exception e) {
				writeLogging("", "Bestand niet aanwezig in werk-directory");
			}

			if (this.currentEditableDocument.getDocumentAction().startsWith("move")) {
				try {
					File fileFrom = new File(
							jtDownloadsFolder.getText() + jtSeperator.getText() + jtDocumentSearchFor.getText());
					File fileTo = new File(jtWorkFolder.getText() + jtSeperator.getText() + jtDocumentname.getText());

					if (fileFrom.renameTo(fileTo)) {
						fileFrom.delete();
						writeLogging("", "Bestand staat nu in de werk-directory.");
					} else {
						writeLogging("", "Bestand verplaatsen naar de werk-directory is mislukt.");
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			if (this.currentEditableDocument.getDocumentAction().endsWith("open")) {
				executeAction("document_open");
				jlStatus.setText("Document geopend in client applicatie.");
			}

			if (newDocument) {

				try {
					File file = new File(jtWorkFolder.getText() + jtSeperator.getText()
							+ this.currentEditableDocument.getDocumentname());
					FileInputStream fileInputStream = new FileInputStream(file);
					MultipartFile multipartFile = new MockMultipartFile(this.currentEditableDocument.getDocumentname(),
							fileInputStream);

					downloadedEditableDocument.setDocument(multipartFile.getBytes());

				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}

				this.listOfEditableDocuments.add(downloadedEditableDocument);

			}

			setJLInfoText();

		}
	}

	public void startWorkWatcherThread() {

		workWatcherThread = new Thread() {
			public void run() {
				try {
					processWorkNotifications();
				} catch (Exception ie) {
					ie.printStackTrace();
				}
				workWatcherThread = null;
			}
		};
		workWatcherThread.start();

	}

	public void processWorkNotifications() {

		try {

			Path workDirectory = Paths.get(jtWorkFolder.getText());
			workWatcher = FileSystems.getDefault().newWatchService();
			WatchKey workKey = workDirectory.register(workWatcher, StandardWatchEventKinds.ENTRY_MODIFY);

			while ((workKey = workWatcher.take()) != null) {
				for (WatchEvent<?> event : workKey.pollEvents()) {
					writeLogging("Worker event kind", event.kind() + ". (file affected: " + event.context() + ")");
					processWorkEvent(event.context().toString());

				}
				workKey.reset();
			}

		} catch (IOException ioe) {
			ioe.printStackTrace();
		} catch (InterruptedException ie) {
			ie.printStackTrace();
		}

	}

	public void processWorkEvent(String documentnameWork) {

		try {

			int volgnummer = 0;

			for (volgnummer = 0; volgnummer < listOfEditableDocuments.size(); volgnummer++) {
				EditableDocument editableDocumentFromList = listOfEditableDocuments.get(volgnummer);
				writeLogging("Naam uit de lijst", editableDocumentFromList.getDocumentname());
				writeLogging("Werk-document", documentnameWork);
				if (documentnameWork.equals(editableDocumentFromList.getDocumentname())) {
					break;
				}
			}

			if (volgnummer == listOfEditableDocuments.size()) {
				jlStatus.setText(
						"Opgeslagen document in werk-directory komt niet voor in lijst van onderhanden documenten");
				return;
			}

			EditableDocument editableDocument = listOfEditableDocuments.get(volgnummer);

			writeLogging("Huidig document", editableDocument.getDocumentname());
			writeLogging("Acties voor document terugplaatsen", editableDocument.getId());

			try {
				File file = new File(jtWorkFolder.getText() + jtSeperator.getText() + documentnameWork);
				FileInputStream fileInputStream = new FileInputStream(file);
				MultipartFile multipartFile = new MockMultipartFile(documentnameWork, fileInputStream);

				if (Arrays.equals(editableDocument.getDocument(), multipartFile.getBytes())) {
					writeLogging("", "Document is gelijk aan download, upload is niet nodig");
					jlStatus.setText("Document is gelijk aan download");
					return;
				}

				editableDocument.setDocument(multipartFile.getBytes());

			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

			String returnValue = RESTSendDocument.sendWithJsonb(jtURL.getText(), editableDocument,
					jtWorkFolder.getText(), documentnameWork);

			listOfEditableDocuments.set(volgnummer, editableDocument);

			jlStatus.setText("Document teruggeplaatst");

			writeLogging("Resultaat van upload", returnValue);

		} catch (Exception e) {
			writeLogging("", "Werk-document upload heeft een fout ...");
			e.printStackTrace();
		}

	}

	protected JComponent createButtons() {

		JPanel panel = new JPanel(new FlowLayout(FlowLayout.TRAILING));

		JButton button = new JButton("Openen document");
		button.addActionListener(this);
		button.setBackground(Color.GRAY);
		button.setActionCommand("document_open");
		panel.add(button);

		button = new JButton("Selecteer download folder");
		button.addActionListener(this);
		button.setActionCommand("download_folder");
		panel.add(button);

		button = new JButton("Selecteer work folder");
		button.addActionListener(this);
		button.setActionCommand("work_folder");
		panel.add(button);

		button = new JButton("Wissel logging");
		button.addActionListener(this);
		button.setActionCommand("switch_logging");
		panel.add(button);

//		button = new JButton("Lees instellingen");
//		button.addActionListener(this);
//		button.setActionCommand("config_get");
//		panel.add(button);
//
//		button = new JButton("Opslaan instellingen");
//		button.addActionListener(this);
//		button.setActionCommand("config_save");
//		panel.add(button);

		button = new JButton("Afsluiten");
		button.addActionListener(this);
		button.setActionCommand("application_quit");
		panel.add(button);

		panel.setBorder(BorderFactory.createEmptyBorder(0, 0, GAP - 5, GAP - 5));

		return panel;

	}

	protected JComponent createStatusBar() {

		JPanel statusBar = new JPanel(new FlowLayout(FlowLayout.LEFT));
		statusBar.setBorder(BorderFactory.createMatteBorder(2, 0, 0, 0, Color.GRAY));
		statusBar.setBackground(Color.BLACK);

		jlStatus = new JLabel();
		jlStatus.setForeground(Color.LIGHT_GRAY);
		jlStatus.setText("Programma is gereed ...");

		jlInfo.setForeground(Color.RED);
		jlInfo.setAlignmentX(RIGHT_ALIGNMENT);

		setJLInfoText();

		statusBar.add(jlStatus, BorderLayout.WEST);
		statusBar.add(jlInfo, BorderLayout.EAST);

		return statusBar;

	}

	protected void setJLInfoText() {
		jlInfo.setText(" (Seperator: " + jtSeperator.getText() + "  Aantal open: " + listOfEditableDocuments.size()
				+ "  Logging: " + jtLogging.getText() + ") ");
	}

	public void actionPerformed(ActionEvent e) {
		executeAction(e.getActionCommand());
	}

	public void executeAction(String action) {

		if ("document_open".equals(action)) {

			try {
				if (Desktop.isDesktopSupported()) {
					Desktop.getDesktop()
							.open(new File(jtWorkFolder.getText() + jtSeperator.getText() + jtDocumentname.getText()));
				}
			} catch (IOException ioe) {
				ioe.printStackTrace();
			}

		} else if ("download_folder".equals(action)) {

			jfDownloadsFolder = new JFileChooser();
			jfDownloadsFolder.setCurrentDirectory(new java.io.File(jtDownloadsFolder.getText()));
			jfDownloadsFolder.setDialogTitle(sDownloadsFolder);
			jfDownloadsFolder.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

			jfDownloadsFolder.setAcceptAllFileFilterUsed(false);

			if (jfDownloadsFolder.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
				writeLogging("getCurrentDirectory()", "" + jfDownloadsFolder.getCurrentDirectory());
				writeLogging("getSelectedFile()", "" + jfDownloadsFolder.getSelectedFile());

				jtDownloadsFolder.setText(jfDownloadsFolder.getSelectedFile().getAbsolutePath());

				saveConfig();

			} else {
				writeLogging("", "Geen keuze gemaakt");
			}

		} else if ("work_folder".equals(action)) {

			jfWorkFolder = new JFileChooser();
			jfWorkFolder.setCurrentDirectory(new java.io.File(jtWorkFolder.getText()));
			jfWorkFolder.setDialogTitle(sWorkFolder);
			jfWorkFolder.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

			jfWorkFolder.setAcceptAllFileFilterUsed(false);

			if (jfWorkFolder.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
				writeLogging("getCurrentDirectory()", "" + jfWorkFolder.getCurrentDirectory());
				writeLogging("getSelectedFile()", "" + jfWorkFolder.getSelectedFile());

				jtWorkFolder.setText(jfWorkFolder.getSelectedFile().getAbsolutePath());

				saveConfig();

			} else {
				writeLogging("", "Geen keuze gemaakt");
			}

		} else if ("switch_logging".equals(action)) {

			if ("true".equals(jtLogging.getText())) {
				jtLogging.setText("false");
			} else {
				jtLogging.setText("true");
			}

			saveConfig();

		} else if ("config_get".equals(action)) {
			getConfig();

		} else if ("config_save".equals(action)) {
			saveConfig();

		} else if ("application_quit".equals(action)) {
			System.exit(0);

		} else {
		}

	}

	private void getConfig() {
		
		jtDownloadsFolder.setText("");
		jtWorkFolder.setText("");
		jtLogging.setText("");

		ClientProperties cp = new ClientProperties();
		Configuration configuration = cp.getConfig();

		try {
			jtDownloadsFolder.setText((String) configuration.getProperty("client.downloadsfolder"));
			jtWorkFolder.setText((String) configuration.getProperty("client.workfolder"));
			jtLogging.setText((String) configuration.getProperty("client.logging"));
		} catch (Exception ce) {
		}

		setJLInfoText();

	}
	private void saveConfig() {

		ClientProperties cp = new ClientProperties();
		Configuration configuration = cp.getConfig();

		try {

			configuration.setProperty("client.downloadsfolder", jtDownloadsFolder.getText());
			configuration.setProperty("client.workfolder", jtWorkFolder.getText());
			configuration.setProperty("client.logging", jtLogging.getText());

			cp.setConfig(configuration);

		} catch (Exception ce) {
		}

		setJLInfoText();

	}

	private void writeLogging(String label, String message) {
		LogWriter.write(label, message);
	}

	private void writeLogging(String label, int message) {
		LogWriter.write(label, "" + message);
	}

	private void writeLogging(String label, long message) {
		LogWriter.write(label, "" + message);
	}

	public void focusGained(FocusEvent e) {
		Component c = e.getComponent();
		if (c instanceof JFormattedTextField) {
			selectItLater(c);
		} else if (c instanceof JTextField) {
			((JTextField) c).selectAll();
		}
	}

	protected void selectItLater(Component c) {
		if (c instanceof JFormattedTextField) {
			final JFormattedTextField ftf = (JFormattedTextField) c;
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					ftf.selectAll();
				}
			});
		}
	}

	public void focusLost(FocusEvent e) {
	}

	protected JComponent createEntryFields() {

		JPanel panel = new JPanel(new SpringLayout());

		String[] labelStrings = { "Gebruikte URL", "Document", "Document sleutel", "Download-directory",
				"Werk-directory" };

		JLabel[] labels = new JLabel[labelStrings.length];
		JComponent[] fields = new JComponent[labelStrings.length];
		int fieldNum = 0;

		jtURL = new JTextField();
		jtURL.setEditable(false);
		jtURL.setEnabled(false);

		fields[fieldNum++] = jtURL;

		jtDocumentname = new JTextField();
		jtDocumentname.setEditable(false);
		jtDocumentname.setEnabled(false);

		fields[fieldNum++] = jtDocumentname;

		jtDocumentSearchFor = new JTextField();
		jtDocumentSearchFor.setEditable(false);
		jtDocumentSearchFor.setEnabled(false);

		fields[fieldNum++] = jtDocumentSearchFor;

		jtDownloadsFolder = new JTextField();
		jtDownloadsFolder.setText("");
		jtDownloadsFolder.setEditable(false);

		jtWorkFolder = new JTextField();
		jtWorkFolder.setText("");
		jtWorkFolder.setEditable(false);

		getConfig();

		fields[fieldNum++] = jtDownloadsFolder;
		fields[fieldNum++] = jtWorkFolder;
		
		for (int i = 0; i < labelStrings.length; i++) {
			labels[i] = new JLabel(labelStrings[i], JLabel.TRAILING);
			labels[i].setLabelFor(fields[i]);
			panel.add(labels[i]);
			panel.add(fields[i]);

			// Add listeners to each field.
			JTextField tf = null;
			tf = (JTextField) fields[i];
			tf.addActionListener(this);
			tf.addFocusListener(this);

		}

		startDownloadWatcherThread();
		startWorkWatcherThread();

		SpringUtilities.makeCompactGrid(panel, labelStrings.length, 2, GAP, GAP, GAP, GAP / 2);

		return panel;

	}

	private static void createAndShowGUI() {
		JFrame frame = new JFrame("Open/bewerk document");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.add(new DocumentClient());
		frame.pack();
		frame.setVisible(true);
		frame.setState(JFrame.ICONIFIED);
	}

	public static void main(String[] args) {
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}