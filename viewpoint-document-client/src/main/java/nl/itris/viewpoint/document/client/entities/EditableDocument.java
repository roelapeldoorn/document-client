package nl.itris.viewpoint.document.client.entities;

import javax.json.bind.annotation.JsonbCreator;
import javax.json.bind.annotation.JsonbProperty;
import javax.json.bind.annotation.JsonbTransient;

public class EditableDocument {

	private long id;
	private byte[] document;
	private String documentname;
	private String documenttype;
	private long documentsize;
	private String documentnameDownload;
	private String documentAction;
	private String documentOriginalTable;
	private long documentOriginalID;
	private boolean documentupdated;
	

	// Object property that does not map to a JSON
    @JsonbTransient
    public boolean legendary = true;

    public EditableDocument() {

    }

    @JsonbCreator
    public EditableDocument(
      @JsonbProperty("id") long id,
      @JsonbProperty("document") byte[] document,
      @JsonbProperty("documentname") String documentname,
      @JsonbProperty("documenttype") String documenttype,
      @JsonbProperty("documentsize") long documentsize,
      @JsonbProperty("documentnameDownload") String documentnameDownload,
      @JsonbProperty("documentAction") String documentAction,
      @JsonbProperty("documentOriginalTable") String documentOriginalTable,
      @JsonbProperty("documentOriginalID") long documentOriginalID,
      @JsonbProperty("documentupdated") boolean documentupdated) {

      this.id = id;
      this.document = document;
      this.documentname = documentname;
      this.documenttype = documenttype;
      this.documentsize = documentsize;
      this.documentnameDownload = documentnameDownload;
      this.documentAction = documentAction;
      this.documentOriginalTable = documentOriginalTable;
      this.documentOriginalID = documentOriginalID;
      this.documentupdated = documentupdated;
      
    }

    @Override
    public String toString() {
      return document + " is van type " + documenttype;
    }

    public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public byte[] getDocument() {
		return document;
	}
	public void setDocument(byte[] document) {
		this.document = document;
	}
	public String getDocumentname() {
		return documentname;
	}
	public void setDocumentname(String documentname) {
		this.documentname = documentname;
	}
	public String getDocumenttype() {
		return documenttype;
	}
	public void setDocumenttype(String documenttype) {
		this.documenttype = documenttype;
	}
	public long getDocumentsize() {
		return documentsize;
	}
	public void setDocumentsize(long documentsize) {
		this.documentsize = documentsize;
	}
	public String getDocumentnameDownload() {
		return documentnameDownload;
	}
	public void setDocumentnameDownload(String documentnameDownload) {
		this.documentnameDownload = documentnameDownload;
	}
	public String getDocumentOriginalTable() {
		return documentOriginalTable;
	}
	public void setDocumentOriginalTable(String documentOriginalTable) {
		this.documentOriginalTable = documentOriginalTable;
	}
	public long getDocumentOriginalID() {
		return documentOriginalID;
	}
	public void setDocumentOriginalID(long documentOriginalID) {
		this.documentOriginalID = documentOriginalID;
	}
	public boolean isDocumentupdated() {
		return documentupdated;
	}
	public void setDocumentupdated(boolean documentupdated) {
		this.documentupdated = documentupdated;
	}
	public boolean isLegendary() {
		return legendary;
	}
	public void setLegendary(boolean legendary) {
		this.legendary = legendary;
	}
	public String getDocumentAction() {
		return documentAction;
	}
	public void setDocumentAction(String documentAction) {
		this.documentAction = documentAction;
	}
    
}
