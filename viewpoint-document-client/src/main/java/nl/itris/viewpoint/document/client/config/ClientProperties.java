package nl.itris.viewpoint.document.client.config;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.PropertiesConfiguration;

public class ClientProperties {

	public Configuration getConfig() {

		try {
			return new PropertiesConfiguration("config/client.properties");
		} catch (Exception e) {
			
		}
		
		return null;
		
	}
	
	public void setConfig(Configuration configuration) {

		try {

			PropertiesConfiguration propertiesConfiguration = new PropertiesConfiguration("config/client.properties");
			
			propertiesConfiguration.setProperty("client.downloadsfolder", configuration.getProperty("client.downloadsfolder"));
			propertiesConfiguration.setProperty("client.workfolder", configuration.getProperty("client.workfolder"));
			propertiesConfiguration.setProperty("client.logging", configuration.getProperty("client.logging"));
						
			propertiesConfiguration.save();
			
		} catch (Exception e) {
			
		}
		
	}
}
