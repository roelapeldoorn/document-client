package nl.itris.viewpoint.document.client.programme;

import org.apache.commons.configuration.Configuration;

import nl.itris.viewpoint.document.client.config.ClientProperties;

public class LogWriter {

	public static void write(String label, String message) {
		
		ClientProperties cp = new ClientProperties();
		Configuration configuration = cp.getConfig();

		String loggingOn = "true";
		
		try {
			loggingOn = (String) configuration.getProperty("client.logging");
		} catch (Exception ce) {
		}
		
		if("true".equals(loggingOn)) {
			int l = label.length();
			if (l > 32) {
				label = label.substring(0, 32);
			} else {
				label = label + "                                ".substring(l);
			}
			
			System.out.println(label + " : " + message);
			
		}
		
	}
	
}
