package nl.itris.viewpoint.document.client.rest;

import java.io.File;
import java.nio.file.FileSystems;

import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import nl.itris.viewpoint.document.client.entities.EditableDocument;
import nl.itris.viewpoint.document.client.programme.LogWriter;

public class RESTSendDocument {
	
	public static String sendWithJsonb(String targetUrl, EditableDocument editableDocument, String workFolder, String changedDocumentName) {

		String returnValue = new String();
		
		if (!changedDocumentName.equals(editableDocument.getDocumentname())) {
			return "Dit is niet het onderhanden document. Er wordt niet opgeslagen.";
		}
		
		try {
			
			FileSystemResource documentchanged = new FileSystemResource(new File(workFolder + FileSystems.getDefault().getSeparator() + changedDocumentName));
			
			MultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
			map.add("document", documentchanged);
			map.add("documentid", String.valueOf(editableDocument.getId()));
			
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.MULTIPART_FORM_DATA);
			
			HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(map, headers);
			
			RestTemplate restTemplate = new RestTemplate();
			
			targetUrl = targetUrl + "REST/documents/upload";
			
			LogWriter.write("URL send", targetUrl);
			
			
			ResponseEntity<String> response = restTemplate.exchange(targetUrl, HttpMethod.POST, requestEntity, String.class);
			
			returnValue = "status: " + response.getStatusCodeValue() + " / opmerking: " + response.getBody();
									
		} catch (Exception e) {
			LogWriter.write("Fout (REST send)", e.getMessage());
			e.printStackTrace();
		}
	    
	    return returnValue;

	}

}
